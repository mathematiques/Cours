# Les vecteurs en première STI2D.

## Rappels de 2nde

Déterminer graphiquement les coordonnées d'un vecteur :

[![texte](http://img.youtube.com/vi/yk_tETEQx6U/0.jpg)](https://www.youtube.com/watch?v=yk_tETEQx6U "Déterminer graphiquement les coordonnées d'un vecteur")

Déterminer par des calculs les coordonnées d'un vecteur :

[![texte](http://img.youtube.com/vi/El1M1s9pHhY/0.jpg)](https://www.youtube.com/watch?v=El1M1s9pHhY "Déterminer par des calculs les coordonnées d'un vecteur")

Déterminer les projeté orthogonal d'un point ou d'un vecteur sur une droite :

[![texte](http://img.youtube.com/vi/jDfYKV2fgpo/0.jpg)](https://www.youtube.com/watch?v=jDfYKV2fgpo "Déterminer les projeté orthogonal d'un point ou d'un vecteur sur une droite")

## 1ère : Le produit scalaire

Pourquoi calculer un produit scalaire ? :

[![texte](http://img.youtube.com/vi/3Pw3rCIrKkc/0.jpg)](https://www.youtube.com/watch?v=3Pw3rCIrKkc "Pourquoi calculer un produit scalaire ?")

Il faut connaître trois méthodes pour calculer un produit scalaire. On choisira l'une ou l'autre en fonction des données de l'exercice à traiter :

1. Avec le projeté orthogonal :

    [![texte](http://img.youtube.com/vi/zGbtdpJGMN4/0.jpg)](https://www.youtube.com/watch?v=zGbtdpJGMN4 "Avec le projeté orthogonal")

2. Avec le cosinus de l'angle :

    [![texte](http://img.youtube.com/vi/CYZ05pLcays/0.jpg)](https://www.youtube.com/watch?v=CYZ05pLcays "Avec le cosinus de l'angle")
    
3. Avec les coordonnées (dans un repère **orthonormé** uniquement) :

    [![texte](http://img.youtube.com/vi/_WahO6htNmg/0.jpg)](https://www.youtube.com/watch?v=_WahO6htNmg "Avec les coordonnées")
